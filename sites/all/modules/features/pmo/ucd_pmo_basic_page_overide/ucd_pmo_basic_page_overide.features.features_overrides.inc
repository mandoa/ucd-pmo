<?php
/**
 * @file
 * ucd_pmo_basic_page_overide.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ucd_pmo_basic_page_overide_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_base
  $overrides["field_base.field_ucd_main_photo.foreign keys"]["DELETED"] = TRUE;

  // Exported overrides for: field_instance
  $overrides["field_instance.node-ucd_page-field_ucd_main_photo.display|default|module"]["DELETED"] = TRUE;
  $overrides["field_instance.node-ucd_page-field_ucd_main_photo.display|default|settings|image_link"]["DELETED"] = TRUE;
  $overrides["field_instance.node-ucd_page-field_ucd_main_photo.display|default|settings|image_style"]["DELETED"] = TRUE;
  $overrides["field_instance.node-ucd_page-field_ucd_main_photo.display|default|type"] = 'hidden';

  // Exported overrides for: image
  $overrides["image.main_photo.effects|0|data|height"] = 200;
  $overrides["image.main_photo.effects|0|data|width"] = 1360;

 return $overrides;
}
