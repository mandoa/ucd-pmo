<?php
/**
 * @file
 * ucd_pmo_basic_page_overide.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ucd_pmo_basic_page_overide_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_field_default_field_bases_alter().
 */
function ucd_pmo_basic_page_overide_field_default_field_bases_alter(&$data) {
  if (isset($data['field_ucd_main_photo'])) {
    unset($data['field_ucd_main_photo']['foreign keys']);
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function ucd_pmo_basic_page_overide_field_default_field_instances_alter(&$data) {
  if (isset($data['node-ucd_page-field_ucd_main_photo'])) {
    $data['node-ucd_page-field_ucd_main_photo']['display']['default']['type'] = 'hidden'; /* WAS: 'image' */
    unset($data['node-ucd_page-field_ucd_main_photo']['display']['default']['module']);
    unset($data['node-ucd_page-field_ucd_main_photo']['display']['default']['settings']['image_link']);
    unset($data['node-ucd_page-field_ucd_main_photo']['display']['default']['settings']['image_style']);
  }
}

/**
 * Implements hook_image_styles_alter().
 */
function ucd_pmo_basic_page_overide_image_styles_alter(&$data) {
  if (isset($data['main_photo'])) {

  if (!isset($data['main_photo']['storage']) || $data['main_photo']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['main_photo']['effects'][0]['data']['height'] = 200;
  }

  if (!isset($data['main_photo']['storage']) || $data['main_photo']['storage'] == IMAGE_STORAGE_DEFAULT) {
    $data['main_photo']['effects'][0]['data']['width'] = 1360;
  }
  }
}
