<?php
/**
 * @file
 * ucd_pmo_user_editing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucd_pmo_user_editing_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
