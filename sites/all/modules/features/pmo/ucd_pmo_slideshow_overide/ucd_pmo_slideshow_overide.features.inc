<?php
/**
 * @file
 * ucd_pmo_slideshow_overide.features.inc
 */

/**
 * Implements hook_image_styles_alter().
 */
function ucd_pmo_slideshow_overide_image_styles_alter(&$data) {
  if (isset($data['slideshow'])) {

  if (!isset($data['slideshow']['storage']) || $data['slideshow']['storage'] == IMAGE_STORAGE_DEFAULT) {
    unset($data['slideshow']['effects'][0]);
  }
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function ucd_pmo_slideshow_overide_views_default_views_alter(&$data) {
  if (isset($data['ucd_slideshow'])) {
    $data['ucd_slideshow']->display['default']->display_options['style_options']['default_row_class'] = FALSE; /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_options']['row_class_special'] = FALSE; /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_options']['skin_info'] = array(
      'class' => 'default',
      'name' => 'Default',
      'module' => 'views_slideshow',
      'path' => '',
      'stylesheets' => array(),
    ); /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_options']['slideshow_skin'] = 'default'; /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_options']['slideshow_type'] = 'flexslider_views_slideshow'; /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_options']['widgets'] = array(
      'top' => array(
        'views_slideshow_pager' => array(
          'weight' => 1,
          'type' => 'views_slideshow_pager_fields',
          'views_slideshow_pager_fields_fields' => array(
            'field_ucd_slideshow_subtitle' => 0,
            'title' => 0,
          ),
        ),
        'views_slideshow_controls' => array(
          'weight' => 1,
          'type' => 'views_slideshow_controls_text',
        ),
        'views_slideshow_slide_counter' => array(
          'weight' => 1,
        ),
      ),
      'bottom' => array(
        'views_slideshow_pager' => array(
          'weight' => 1,
          'type' => 'views_slideshow_pager_fields',
          'views_slideshow_pager_fields_fields' => array(
            'field_ucd_slideshow_subtitle' => 0,
            'title' => 0,
          ),
        ),
        'views_slideshow_controls' => array(
          'weight' => 1,
          'type' => 'views_slideshow_controls_text',
        ),
        'views_slideshow_slide_counter' => array(
          'weight' => 1,
        ),
      ),
    ); /* WAS: '' */
    $data['ucd_slideshow']->display['default']->display_options['style_plugin'] = 'slideshow'; /* WAS: 'flexslider' */
    unset($data['ucd_slideshow']->display['default']->display_options['fields']['title']);
    unset($data['ucd_slideshow']->display['default']->display_options['style_options']['captionfield']);
    unset($data['ucd_slideshow']->display['default']->display_options['style_options']['optionset']);
  }
}
