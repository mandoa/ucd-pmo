<?php
/**
 * @file
 * ucd_pmo_slideshow_overide.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ucd_pmo_slideshow_overide_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: image
  $overrides["image.slideshow.effects|0"]["DELETED"] = TRUE;

  // Exported overrides for: views_view
  $overrides["views_view.ucd_slideshow.display|default|display_options|fields|title"]["DELETED"] = TRUE;
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|captionfield"]["DELETED"] = TRUE;
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|default_row_class"] = FALSE;
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|optionset"]["DELETED"] = TRUE;
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|row_class_special"] = FALSE;
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|skin_info"] = array(
    'class' => 'default',
    'name' => 'Default',
    'module' => 'views_slideshow',
    'path' => '',
    'stylesheets' => array(),
  );
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|slideshow_skin"] = 'default';
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|slideshow_type"] = 'flexslider_views_slideshow';
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_options|widgets"] = array(
    'top' => array(
      'views_slideshow_pager' => array(
        'weight' => 1,
        'type' => 'views_slideshow_pager_fields',
        'views_slideshow_pager_fields_fields' => array(
          'field_ucd_slideshow_subtitle' => 0,
          'title' => 0,
        ),
      ),
      'views_slideshow_controls' => array(
        'weight' => 1,
        'type' => 'views_slideshow_controls_text',
      ),
      'views_slideshow_slide_counter' => array(
        'weight' => 1,
      ),
    ),
    'bottom' => array(
      'views_slideshow_pager' => array(
        'weight' => 1,
        'type' => 'views_slideshow_pager_fields',
        'views_slideshow_pager_fields_fields' => array(
          'field_ucd_slideshow_subtitle' => 0,
          'title' => 0,
        ),
      ),
      'views_slideshow_controls' => array(
        'weight' => 1,
        'type' => 'views_slideshow_controls_text',
      ),
      'views_slideshow_slide_counter' => array(
        'weight' => 1,
      ),
    ),
  );
  $overrides["views_view.ucd_slideshow.display|default|display_options|style_plugin"] = 'slideshow';

 return $overrides;
}
