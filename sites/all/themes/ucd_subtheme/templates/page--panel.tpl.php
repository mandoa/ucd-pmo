<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<!-- ******************** Header ******************** -->
<div id="header" class="clearfix">
  <div class="outer_wrap">
    <div class="inner_wrap">
      <div id="header_content" class="clearfix">
        <div id="ucdavis_logo"><a href="http://www.ucdavis.edu/"><img alt="UC Davis home page" src="https://cmsresources.ucdavis.edu/cms_v2/images/common/ucdavis_logo_hdr_blue.png" /></a></div>
        <div id="menu-toggle"><a href="#" class="btn-menu bar_1">Menu</a></div>
        <?php print render($page['header_top']); ?>
      </div>
    </div>
  </div>
</div>
<!-- ******************** End header ******************** -->

<!-- ******************** Site identification ******************** -->
<div id="site_title">
  <div class="outer_wrap">
    <div class="inner_wrap">
      <div id="title_area_content" role="banner">
        <?php if ($logo && $site_name): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="logo" />
          </a>
        <?php endif; ?>

        <?php if ($site_name): ?>
        <div class="heading" id="title">
          <h1><?php print $site_name; ?></h1>
        <?php elseif ($logo): ?>
        <div class="logo" id="title">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="logo" />
          </a>
        <?php endif; ?>
          <?php if ($header_image_path): ?>
            <img alt="" src="<?php print $base_path . $header_image_path; ?>" />
          <?php endif; ?>
        </div>

        <?php print render($page['header']); ?>
        <div class="clearer"><!-- Needs to be here to prevent title area collapsing --></div>
      </div>
    </div>
  </div>
</div>
<!-- ******************** End site identification ******************** -->

<!-- ******************** Primary navigation ******************** -->

<div id="nav" role="navigation">
  <div class="nav_background bar_1">
    <div class="outer_wrap">
      <div class="inner_wrap">
        <div class="nav_content clearfix">
          <?php if ($main_menu || $secondary_menu): ?>
          <?php
            print theme('links__system_main_menu', array(
              'links' => $main_menu,
              'attributes' => array(
                'id' => 'main-menu',
                'class' => array('primary_nav'),
              ),
            ));
          ?>
          <?php
            print theme('links__system_secondary_menu', array(
              'links' => $secondary_menu,
              'attributes' => array(
                'id' => 'secondary-menu',
                'class' => array('links', 'inline', 'clearfix'),
              ),
              'heading' => t('Secondary menu'),
            ));
          ?>
          <?php endif; ?>
          <?php print render($page['menu']); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ******************** End primary navigation ******************** -->

  <!-- ******************** Hero Banner ******************** -->
  <?php if ($page['hero_banner']): ?>
    <div id="hero_banner_container">
      <?php print render($page['hero_banner']); ?>
    </div>
  <?php endif; ?>
  <!-- ******************** End Hero Banner ******************** -->

<!-- ******************** Breadcrumb navigation ******************** -->
<?php //if ($breadcrumb): ?>
<!--<div id="breadcrumbs_container">-->
<!--  <div class="outer_wrap">-->
<!--    <div class="inner_wrap">-->
<!--      <div id="breadcrumbs_content">-->
<!--        <div id="breadcrumbs" role="navigation">-->
<!--          --><?php //print $breadcrumb; ?>
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->
<?php //endif; ?>
<!-- ******************** End breadcrumb navigation ******************** -->

<!-- ******************** Main page columns ******************** -->
<div id="main" class="<?php print $classes; ?>">
  <div class="outer_wrap">
    <div class="inner_wrap">
      <div id="main_content">

        <?php print $messages; ?>

        <!-- ********** Page title ********** -->
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><div id="page_title"><h2><?php print $title; ?></h2></div><?php endif; ?>
        <?php print render($title_suffix); ?>

        <!-- ********** Tabs ********** -->
        <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

        <?php //print $feed_icons; ?>

        <?php print render($page['content']); ?>

      </div>
    </div>
  </div>
</div>
<!-- ******************** End main page columns ******************** -->

<!-- ******************** Footer ******************** -->
<div id="footer">
  <div class="outer_wrap">
    <div class="inner_wrap">
      <div id="footer_content">
        <!-- ********** Standard footer content ********** -->
        <div id="standard_footer" role="contentinfo">
          <?php print render($page['footer']); ?>
          <?php if ($ucd_copyright): ?>
            <p>Copyright &#169; The Regents of the University of California, Davis campus. All rights reserved.</p>
          <?php endif; ?>
        </div>
        <!-- ********** End standard footer content ********** -->
      </div>
    </div>
  </div>
</div>
<!-- ******************** End footer ******************** -->

