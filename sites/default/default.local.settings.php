<?php
/**
 * This file is for local site configuration settings.
 *
 * To use this, copy this file and rename it to
 * local.settings.php
 *
 * Next, uncomment and add the following lines of code to the settings.php file.
 */
#if (!isset($_SERVER['PANTHEON_ENVIRONMENT'])) {
#  if (file_exists(dirname(__FILE__) . '/local.settings.php')) {
#    require_once(dirname(__FILE__) . '/local.settings.php');
#  }
#}

/**
 * Database settings:
 *
 * The $databases array specifies the database connection or
 * connections that Drupal may use.  Drupal is able to connect
 * to multiple databases, including multiple types of databases,
 * during the same request.
 *
 * Database configuration format:
 * @code
 *   $databases['default']['default'] = array(
 *     'driver' => 'mysql',
 *     'database' => 'databasename',
 *     'username' => 'username',
 *     'password' => 'password',
 *     'host' => 'localhost',
 *     'prefix' => '',
 *   );
 * @endcode
 *
 * Check the settings.php file for more info
 */
$databases = array ();

/**
 * Salt for one-time login links and cancel links, form tokens, etc.
 *
 * This variable will be set to a random value by the installer. All one-time
 * login links will be invalidated if the value is changed. Note that if your
 * site is deployed on a cluster of web servers, you must ensure that this
 * variable has the same value on each server. If this variable is empty, a hash
 * of the serialized database credentials will be used as a fallback salt.
 *
 * For enhanced security, you may set this variable to a value using the
 * contents of a file outside your docroot that is never saved together
 * with any backups of your Drupal files and database.
 *
 * Example:
 *   $drupal_hash_salt = file_get_contents('/home/example/salt.txt');
 *
 */
$drupal_hash_salt = '';

/**
 * Disable cache and css/js aggregation
 * This will override any settings in Performance
 * /admin/config/development/performance
 */
$conf['cache'] = 0;
$conf['block_cache'] = 0;
$conf['preprocess_css'] = 0;
$conf['preprocess_js'] = 0;

/**
 * Local temp file directory
 * The default for use with MAMP
 */
#$conf['file_temporary_path'] = '/Applications/MAMP/tmp/php';

/**
 * Save emails to file
 * This requires the Devel module to be enabled
 */
#$conf['mail_system'] = array(
#  'default-system' => 'DevelMailLog',
#);