api = 2
core = 7.x

; Download the UCD Base install profile and recursively build all its dependencies.
projects[drupal][type] = core
projects[drupal][download][type] = "git"
projects[drupal][download][url] = "git@bitbucket.org:ietwebdev/ucddrops-drupal-7-distribution.git"
