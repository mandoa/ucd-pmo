<?php
/**
 * Implements hook_form_system_theme_settings_alter()
 */
function ucd_webcms_form_system_theme_settings_alter(&$form, $form_state) {

  //add a header image toggle
  $form['theme_settings']['toggle_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Header Image'),
    '#default_value' => theme_get_setting('toggle_header'),
  );
  //add a style sheet allowing the site to become responsive
  $form['theme_settings']['toggle_responsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Responsive Layout'),
    '#default_value' => theme_get_setting('toggle_responsive'),
    '#description' => t('Check here if you want the theme to become "Responsive" allowing the layout to adjust to smaller screen sizes like a smart phone.'),
  );

  //get the header image path
  $header_image_path = theme_get_setting('header_image_path');

  // Define the settings-related FormAPI elements.
  $form['header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header Image Settings')
   );
  $form['header']['header_image_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the default header image'),
      '#default_value' => theme_get_setting('header_image_default'),
      '#tree' => FALSE,
      '#description' => t('Check here if you want the theme to use the header image supplied with it.'),
    );
  $form['header']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the header image settings when using the default header image.
      'invisible' => array(
        'input[name="header_image_default"]' => array('checked' => TRUE),
      ),
    ),
  );
  if (!empty($header_image_path)) {
    //$files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
    $path = image_style_url('thumbnail', $header_image_path);
    $form['header']['settings']['header_image_preview'] = array(
      '#markup' => '<div style="float:left; margin-right: 1em;">' . theme('image', array('path' => $path)) . '</div>',
    );
  }
  $form['header']['settings']['header_image'] = array(
    '#type' => 'file',
    '#title' => t('Upload Header image'),
    '#maxlength' => 40,
    '#description' => t('For best results upload an image with dimensions of 1,400px × 200px.'),
  );
  $form['header']['settings']['header_image_path'] = array(
    '#type' => 'hidden',
    '#default_value' => $header_image_path,
  );

  //CMS color choice
  $form['cms_color'] = array(
    '#type' => 'fieldset',
    '#title' => t('CMS Theme Color Scheme Options'),
  );
  $form['cms_color']['cms_color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Choose the color scheme for this theme'),
    '#default_value' => theme_get_setting('cms_color_scheme'),
    '#options' => array(
      'gunrock_gold' => t('Gunrock - Gold'),
      'gunrock_gold_ruled' => t('Gunrock - Gold, ruled'),
      'gunrock_blue' => t('Gunrock - Blue'),
      'gunrock_white_ruled' => t('Gunrock - White, ruled'),
      'silo_gold_dark-blue' => t('Silo - Gold, Dark Blue'),
      'silo_blue_medium-blue_pale-gold' => t('Silo - Blue, Medium Blue, and Pale Gold'),
      'silo_blue_pale-blue' => t('Silo - Blue, Pale Blue'),
      'silo_blue_medium-blue' => t('Silo - Blue, Medium Blue'),
      'silo_blue_medium-blue_pale-blue' => t('Silo - Blue, Medium Blue, and Pale Blue'),
      'silo_white_pale-gold' => t('Silo - White, Pale Gold'),
      'silo_white_pale-blue' => t('Silo - White, Pale Blue'),
    ),
  );
  //a preview thumbnail which will change when a new theme is selected
  $webcms_path = base_path() . drupal_get_path('theme', 'ucd_webcms');
  $form['cms_color']['cms_color_thumbnail'] = array(
    '#markup' => '<div><img src="' . $webcms_path . '/images/previews/' . theme_get_setting('cms_color_scheme') . '.jpg" id="color-scheme-preview" /></div>',
  );

  //add javascript to make the thumbnail preview change based on the selection
  $js_preview = <<<JS
    jQuery(document).ready(function() {
      var preview = jQuery('#color-scheme-preview');

      jQuery('#edit-cms-color-scheme').bind('change keyup', function() {
        var new_image = '{$webcms_path}/images/previews/' + jQuery(this).val() + '.jpg';
        preview.attr('src', new_image);
      });
    });
JS;
  drupal_add_js($js_preview, 'inline');


  $form['#validate'][] = 'ucd_webcms_header_validate';
  $form['#submit'][] = 'ucd_webcms_header_submit';
}

/*
 * On submit set the header image settings values
 */
function ucd_webcms_header_submit($form, &$form_state) {
  // If the user uploaded a new header image, save it to a permanent location
  // and use it in place of the default theme-provided header image.
  if ($file = $form_state['values']['header_image']) {
    //unset($form_state['values']['header_image']);
    $filename = file_unmanaged_copy($file->uri);
    $form_state['values']['header_image_default'] = 0;
    $form_state['values']['header_image_path'] = $filename;
  }
}

/*
 * Save the uploaded header image to the public file directory
 */
function ucd_webcms_header_validate($form, &$form_state) {
  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded header image.
  $file = file_save_upload('header_image', $validators, FALSE, FILE_EXISTS_REPLACE);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['header_image'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('header_image', t('The header image could not be uploaded.'));
    }
  }
}