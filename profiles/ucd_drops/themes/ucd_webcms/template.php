<?php
/**
 * Implementation of hook_preprocess_html().
 */
function ucd_webcms_preprocess_html(&$variables) {
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, minimum-scale=1',
    )
  );
  // Add header meta tag to head
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  //add a css class to the body if a logo is used
  if (theme_get_setting('toggle_logo')) {
    $variables['classes_array'][] = 'has-logo';
  }
  //add a css class to the body if a header image is used
  if (theme_get_setting('toggle_header')) {
    $variables['classes_array'][] = 'has-header-image';
  }
  //add the chosen CMS color scheme css file
  $variables['cms_color_scheme'] = theme_get_setting('cms_color_scheme') . '.css';
}

/*
 * Preprocess the block variables
 */
function ucd_webcms_preprocess_block(&$variables) {
  //set up block identifier variable
  $block_id = $variables['block']->module . '-' . $variables['block']->delta;

  //if this block is from the Menu Block module
  if ($variables['block']->module == 'menu_block') {
    $variables['classes_array'][] = 'nav_second';
    $variables['attributes_array']['role'][] = 'navigation';
    $variables['title_attributes_array']['class'][] = 'here';
    //add a "here" class to the end of all "active" classes
    $variables['content'] = preg_replace("/active(?!-)/", "active here", $variables['content']);
  }

  //add a user menu toggle button to the top of the main navigation
  if ($variables['block']->region == 'header_top' && $block_id == 'system-user-menu') {
    $button = '<a href="#" id="user-menu-toggle" class="btn-menu bar_1 user-menu-toggle ir">User Menu</a>';
    $variables['content'] = $button . $variables['content'];
  }

  //add a class for the search form when in the header_top
  if ($variables['block']->region == 'header_top' && $block_id == 'search-form') {
    $variables['classes_array'][] = 'block-search-form';
  }

}

  /*
   * Preprocess the views_view_unformatted variables
   */
function ucd_webcms_preprocess_views_view_unformatted(&$variables) {
  //if this block is from the "Main Image" view
  if ($variables['view']->name == 'main_image') {
    $variables['classes_array'][0] .= ' content_box image top_item';
  }
}

//add the page title to the breadcrumb
function ucd_webcms_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (empty($breadcrumb)) {
    $output = '<ul class="breadcrumbs"><li><span class="here">Home</span></li></ul>';
  } else {
    // Adding the title of the current page to the breadcrumb.
    $breadcrumb[] = drupal_get_title();

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $current = sizeof($breadcrumb);
    $i = 1;

    $output .= '<ul class="breadcrumbs">';
    foreach($breadcrumb as $value) {
      if ($i != $current){
        $output .= '<li class="breadcrumb-' . $i . '">' . $value . ' ' . '</li>';
        $i++;
      } else {
        $output .= '<li><span class="here">' . $value . '</span></li>';
      }
    }
    $output .= '</ul>';
  }
  return $output;
}

/**
 * Implementation of hook_preprocess_page().
 */
function ucd_webcms_preprocess_page(&$variables) {
  //get the header image from the theme settings
  $toggle_header = theme_get_setting('toggle_header');
  $header_image_path = theme_get_setting('header_image_path');
  $header_image_default = theme_get_setting('header_image_default');

  if ($toggle_header) {
    if (!empty($header_image_path) && empty($header_image_default)) {
      $files_dir = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath();
      $variables['header_image_path'] = $files_dir . '/' . file_uri_target($header_image_path);
    } else {
      $variables['header_image_path'] = path_to_theme() . '/images/ucd_webcms_header.jpg';
    }
  } else {
    $variables['header_image_path'] = FALSE;
  }

  //suggest a page-panel tpl file for panel pages
  $panel_page = FALSE;
  if (module_exists('panels') && $panel_page = page_manager_get_current_page()) {
    // add a generic suggestion for all panel pages
    $variables['theme_hook_suggestions'][] = 'page__panel';
    // add the panel page machine name to the template suggestions
    $variables['theme_hook_suggestions'][] = 'page__panel__' . $panel_page['name'];

    //add a css class
    $variables['classes_array'][] = 'page-panel';
  }

  //set classes for the main content area
  $variables['classes_array'][] = ($variables['page']['sidebar_second']) ? 'sidebar-second': 'without-sidebar-second';
  $variables['classes_array'][] = ($variables['page']['sidebar_first']) ? 'sidebar-first': 'without-sidebar-first';
  if (!$variables['page']['sidebar_first'] && !$variables['page']['sidebar_first'] && !$panel_page) {
    $variables['classes_array'][] = 'full-width';
  }

  //add variables for copyright if the copyright block is not set in the footer
  $variables['ucd_copyright'] = (isset($variables['page']['footer']['copyright_copyright'])) ? FALSE : TRUE;

}

/**
 * Implements theme_menu_tree().
 * Add a class to the user menu
 */
function ucd_webcms_menu_tree__user_menu(&$variables) {
  return '<ul id="user-menu" class="user-menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements of hook_css_alter().
 *
 * remove the responsive css file if the checkbox setting is not checked
 */
function ucd_webcms_css_alter(&$css) {
  //check to see if the responsive stylesheet is toggled
  $toggle_responsive = theme_get_setting('toggle_responsive');

  //if the toggle for responsive layout is not checked then remove the
  //the responsive stylesheet
  if (!$toggle_responsive) {
    unset($css[path_to_theme() .'/css/responsive.css']);
  }
}