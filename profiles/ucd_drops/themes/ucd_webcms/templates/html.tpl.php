<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8 ie7" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9 ie8" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"> <!--<![endif]-->
<head profile="<?php print $grddl_profile; ?>">
  <meta content="text/css" http-equiv="Content-Style-Type" />
  <meta content="text/javascript" http-equiv="Content-Script-Type" />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/layout/common.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/layout/level_2.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/print.css" media="print" rel="stylesheet" type="text/css" />
  <!-- Colors -->
  <link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/color/common.css" media="screen" rel="stylesheet" type="text/css" />
  <!-- Custom stylesheets -->
  <link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/color/<?php print $cms_color_scheme; ?>" rel="stylesheet" type="text/css" />
  <!--[if IE 7]><link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/layout/ie7.css" media="screen" rel="stylesheet" type="text/css"/><![endif]-->
  <!--[if lte IE 7]><link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/layout/lte_ie7.css" media="screen" rel="stylesheet" type="text/css"/><![endif]-->
  <!--[if IE 6]><link href="https://cmsresources.ucdavis.edu/cms_v2/css_v2/layout/ie6.css" media="screen" rel="stylesheet" type="text/css"/><![endif]-->
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <!--
      This will show only in text-only browsers and allows the user
      to skip the primary navigation and go directly to the main content of the page
  -->
  <div id="navskip">
    <a href="#main_content">Skip to page content</a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>