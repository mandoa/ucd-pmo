// Sitewide scripts using jquery
(function ($) {

// Open external links in a new window
Drupal.behaviors.externalLinkTarget = {
  attach: function (context, settings) {
    $("a", context).filter(function() {
      return this.hostname && this.hostname !== location.hostname;
    }).attr("target", "_blank");
  }
};

//default text for search bar
Drupal.behaviors.defaultSearchText = {
  attach: function (context, settings) {

    //only add this once on page load
    $('#edit-search-block-form--2', context).once(function(){
      var defaultSearchText = "Search";

      var $this = $(this);

      $this.focus(function() {
        if ($(this).val() == defaultSearchText) {
          $(this).removeClass("search-default-text").val("");
        }
      });

      $this.blur(function() {
        if ($(this).val() == "") {
          $(this).addClass("search-default-text").val(defaultSearchText);
        }
      });

      $this.blur();
    });

  }
};

//Make the menus collapse for Mobile
Drupal.behaviors.mobileMenu = {
  attach: function (context, settings) {

    //set the width breakpoint for becoming mobile
    var mobileWidth = 890;

    var $menu = $('.primary_nav', context);
    var $user_menu = $('#user-menu', context);
    var $search = $('.block-search-form', context);

    //change the menu to superfish if installed
    if (settings.superfish) {
      $menu = $('#superfish-1', context);
    }

    //menu toggle for mobile
    $("#menu-toggle a").click(function() {
      $menu.slideToggle(500);
      $search.toggleClass('is-visible'); //show the search bar
      return false;
    });

    //if using superfish and not less than ie9
    if (!$('html').hasClass('lt-ie9') && settings.superfish) {
      $(window).bind('resize load', function() {
        if ($(document).width() > mobileWidth) {
          //if the sf-menu class is not applied
          if (!$menu.hasClass('sf-menu')) {
            $menu.css('display', 'block');
            //make sure the correct superfish classes are added
            $menu.removeClass('mobile-menu').addClass('sf-menu');
            $('li.menuparent > a').unbind();
            //check to see if supersubs is being used
            if (settings.superfish[1].plugins.supersubs) {
              $menu.supersubs({minWidth: 12, maxWidth: 27, extraWidth: 1});
            }
            $menu.superfish({
              animation: {opacity:'show'},
              speed: 'fast',
              autoArrows: false,
              dropShadows: false});
          }
        } else {
          //if the mobile-menu class is not applied
          if (!$menu.hasClass('mobile-menu')) {
            $menu.hide();
            $search.removeClass('is-visible'); //hide the search block
            //remove superfish ability
            $menu.removeClass('sf-menu sf-shadow').addClass('mobile-menu');
            $('#superfish-1 li, #superfish-1 a').unbind();
            $('#superfish-1 li ul').removeAttr('style');
            //add click events
            $('li.menuparent > a').one('click', function() {
              $(this).next('ul').show();
              return false;
            });
          }
        }
      });

    //not using superfish and not less than ie9
    } else if (!$('html').hasClass('lt-ie9')) {
      $(window).bind('resize load', function() {
        if ($(document).width() > mobileWidth) {
          $menu.css('display', 'block').removeClass('mobile-menu');
        } else {
          $menu.hide().addClass('mobile-menu');
        }
      });
    }

    //user menu toggle for mobile
    if ($("#user-menu-toggle", context).length) {
      $("#user-menu-toggle", context).click(function() {
        $user_menu.slideToggle(250);
        return false;
      });

      if (!$('html').hasClass('lt-ie9')) {
        $(window).bind('resize load', function() {
          if ($(document).width() > mobileWidth) {
            $user_menu.css('display', 'block').removeClass('mobile-menu');
          } else {
            $user_menu.hide().addClass('mobile-menu');
          }
        });
      }
    }

  }
};

})(jQuery); // end jquery enclosure

//initiate the iphone scalefix
MBP.scaleFix();