# Overriding template files
Place your template files in this directory. You can optionally organize them in
subdirectories.

WARNING! If you override template files from the base ucd_webcms theme then be
sure to update these files if the base files have been updated. Otherwise it is
possible that base css and js may not match what is in the overridden templates.
