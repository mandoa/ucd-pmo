name = Default
description = Subtheme of the UCD WebCMS Base Theme. Main regions available are Sidebar First, Content, and Sidebar Second.
screenshot = screenshot.png
engine = phptemplate
core = 7.x

; STYLESHEETS
stylesheets[all][] = css/{{ THEME SANITIZED }}.styles.css

; SCRIPTS
scripts[] = js/{{ THEME SANITIZED }}.behaviors.js

; REQUIRED CORE REGIONS
regions[page_top] = Page Top
regions[page_bottom] = Page Bottom
regions[content] = Content

; ADDITIONAL REGIONS
regions[header_top] = Header Top
regions[header] = Header
regions[menu] = Menu
regions[help] = Help

regions[content] = Content
regions[sidebar_first] = Sidebar first
regions[sidebar_second] = Sidebar second

regions[footer] = Footer

; FEATURE TOGGLES
features[] = logo
features[] = name
features[] = favicon
features[] = main_menu

; THEME SETTINGS (DEFAULTS)
settings[toggle_logo] = '0'
settings[toggle_name] = '1'
settings[toggle_favicon] = '1'
settings[toggle_main_menu] = '1'
settings[toggle_header] = '1'
settings[toggle_responsive] = '1'
settings[default_logo] = '1'
settings[logo_path] = ''
settings[logo_upload] = ''
settings[default_favicon] = '1'
settings[favicon_path] = ''
settings[favicon_upload] = ''
settings[header_image_default] = '1'
settings[header_image_path] = ''
settings[cms_color_scheme] = 'silo_blue_medium-blue_pale-blue'