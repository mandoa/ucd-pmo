<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_2col_20_80_panels_layouts() {
  $items['cms_2col_20_80'] = array(
    'title' => t('2 column 20/80'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_2col_20_80.png',
    'theme' => 'cms-2col-20-80',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
  );

  return $items;
}
