<div class="panel-display cms-2col-20-80 sidebar-first" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <!-- ********** Main content section ********** -->
  <div id="main_section" role="main">
    <!-- Center column (primary content) -->
    <div id="center_column">
      <div id="center_column_inner_wrap">
        <?php print $content['right']; ?>
      </div>
    </div>
    <!-- End center column -->
  </div>
  <!-- ********** End main content ********** -->

  <!-- ********** Main sidebar ********** -->
  <div id="main_sidebar">
    <div id="main_sidebar_inner_wrap">
      <h2 class="semantic_column_header">Secondary navigation and site ownership</h2>
      <?php print $content['left']; ?>
    </div>
  </div>
  <!-- ********** End main sidebar ********** -->
</div>