<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_single_col_panels_layouts() {
  $items['cms_single_col'] = array(
    'title' => t('Single column'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_single_col.png',
    'theme' => 'cms-single-col',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'center' => t('Center'),
    ),
  );

  return $items;
}
