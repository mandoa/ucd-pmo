<div class="panel-display cms-single-col full-width" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <!-- ********** Main content section ********** -->
  <div id="main_section" role="main">
    <!-- Center column (primary content) -->
    <div id="center_column">
      <div id="center_column_inner_wrap">
        <?php print $content['center']; ?>
      </div>
    </div>
    <!-- End center column -->
  </div>
  <!-- ********** End main content ********** -->

</div>