<div class="panel-display cms-3col-33-33-33-stacked" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

    <!-- Top column -->
    <div class="top_column">
      <div class="top_column_inner_wrap inner_wrap">
        <?php print $content['top']; ?>
      </div>
    </div>
    <!-- End top column -->

    <!-- Left column -->
    <div class="column left_column">
      <div class="left_column_inner_wrap inner_wrap">
        <?php print $content['left']; ?>
      </div>
    </div>
    <!-- End left column -->

    <!-- Center column (primary content) -->
    <div class="column center_column">
      <div class="center_column_inner_wrap inner_wrap">
        <?php print $content['center']; ?>
      </div>
    </div>
    <!-- End left column -->

    <!-- Right column -->
    <div class="column right_column">
      <div class="right_column_inner_wrap inner_wrap">
        <?php print $content['right']; ?>
      </div>
    </div>
    <!-- End right column -->

    <!-- Bottom column -->
    <div class="bottom_column">
      <div class="bottom_column_inner_wrap inner_wrap">
        <?php print $content['bottom']; ?>
      </div>
    </div>
    <!-- End bottom column -->

</div>