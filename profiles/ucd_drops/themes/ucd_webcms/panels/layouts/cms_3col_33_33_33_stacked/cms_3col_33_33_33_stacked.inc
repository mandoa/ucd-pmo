<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_3col_33_33_33_stacked_panels_layouts() {
  $items['cms_3col_33_33_33_stacked'] = array(
    'title' => t('3 column 33/33/33 stacked'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_3col_33_33_33_stacked.png',
    'theme' => 'cms-3col-33-33-33-stacked',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );

  return $items;
}
