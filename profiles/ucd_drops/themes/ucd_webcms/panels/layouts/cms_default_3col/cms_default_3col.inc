<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_default_3col_panels_layouts() {
  $items['cms_default_3col'] = array(
    'title' => t('Default 3 column 20/80/20'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_default_3col.png',
    'theme' => 'cms-default-3col',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
  );

  return $items;
}
