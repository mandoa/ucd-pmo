<div class="panel-display cms-2col-75-25 full-width sidebar-second" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <!-- ********** Main content section ********** -->
  <div id="main_section" role="main">
    <!-- Center column (primary content) -->
    <div id="center_column">
      <div id="center_column_inner_wrap">
        <?php print $content['left']; ?>
      </div>
    </div>
    <!-- End center column -->

    <!-- Right column -->
    <div class="column" id="right_column">
      <div id="right_column_inner_wrap">
        <?php print $content['right']; ?>
      </div>
    </div>
    <!-- End right column -->

  </div>
  <!-- ********** End main content ********** -->

</div>