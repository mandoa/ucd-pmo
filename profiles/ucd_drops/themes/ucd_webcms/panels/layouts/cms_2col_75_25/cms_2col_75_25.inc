<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_2col_75_25_panels_layouts() {
  $items['cms_2col_75_25'] = array(
    'title' => t('2 column 75/25'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_2col_75_25.png',
    'theme' => 'cms-2col-75-25',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
  );

  return $items;
}
