<?php

/**
 * Implements hook_panels_layouts().
 */
function ucd_webcms_cms_2col_50_50_stacked_panels_layouts() {
  $items['cms_2col_50_50_stacked'] = array(
    'title' => t('2 column 50/50 stacked'),
    'category' => t('UCD webCMS'),
    'icon' => 'cms_2col_50_50_stacked.png',
    'theme' => 'cms-2col-50-50-stacked',
    'admin css' => '../cms_panels_admin.css',
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );

  return $items;
}
