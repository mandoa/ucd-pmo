<?php

/**
 * Implementation of hook_panels_styles().
 */
$plugin = array(
  'quick_links' => array(
    'title' => t('Quick Links'),
    'description' => t('Block of links'),
    'render pane' => 'quick_links_render_pane',
    'hook theme' => array(
      'quick_links_pane' => array(
        'variables' => array('content' => NULL),
        'path' =>
        drupal_get_path('theme', 'ucd_webcms') . '/panels/styles/quick_links',
        'template' => 'quick-links-pane',
      ),
    ),
  ),
);

/**
 * Render the Pane
 */
function theme_quick_links_render_pane($vars) {
  $content = $vars['content'];
  return theme('quick_links_pane', array('content' => $content));
}

