<div class="panel-pane quick_links <?php print isset($content->css_class) ? $content->css_class : ""; ?>" <?php print isset($content->css_id) ? ' id="' . $content->css_id . '"' : ""; ?>>
  <div class="panel-inside border_box">
    <?php if (isset($content->title)): ?>
      <h3 class="pane-title headline headline_top"><?php print $content->title; ?></h3>
    <?php endif ?>
    <div class="pane-content">
      <?php print render($content->content); ?>
    </div>
  </div>
</div>