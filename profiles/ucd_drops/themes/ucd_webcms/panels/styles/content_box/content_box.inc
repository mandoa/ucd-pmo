<?php

/**
 * Implementation of hook_panels_styles().
 */

$plugin = array(
  'content_box' => array(
    'title' => t('Content Box'),
    'description' => t('Box to highlight content'),
    'render pane' => 'content_box_render_pane',
    'pane settings form' => 'content_box_settings_form',
    'hook theme' => array(
      'content_box_theme_pane' => array(
        'template' => 'content-box-pane',
        'path' =>
        drupal_get_path('theme', 'ucd_webcms') . '/panels/styles/content_box',
        'variables' => array(
          'content' => NULL,
          'settings' => NULL,
        ),
      ),
    ),
  ),
);

/**
 * Render the Pane
 */
function theme_content_box_render_pane($vars) {
  $content = $vars['content'];
  $settings = $vars['settings'];
  return theme('content_box_theme_pane', array(
    'content' => $content,
    'settings' => $settings
  ));
}

function content_box_settings_form($style_settings) {
  $form['border'] = array(
    '#type' => 'select',
    '#title' => t('Border type'),
    '#options' => array(
      'border_box' => t('Bordered'),
      '' => t('No Border'),
    ),
    '#default_value' => (isset($style_settings['border'])) ? $style_settings['border'] : 'border_box',
  );

  $form['strong'] = array(
    '#type' => 'select',
    '#title' => t('Title weight'),
    '#options' => array(
      'headline_strong' => t('Strong'),
      '' => t('Normal'),
    ),
    '#default_value' => (isset($style_settings['strong'])) ? $style_settings['strong'] : '',
  );


  return $form;
}