<div class="panel-pane content_box <?php print isset($content->css_class) ? $content->css_class : ""; ?>" <?php print isset($content->css_id) ? ' id="' . $content->css_id . '"' : ""; ?>>
  <div class="panel-inside <?php echo isset($settings['border']) ? $settings['border'] : ""; ?>">
    <?php if (isset($content->title)): ?>
      <h3 class="pane-title headline headline_top <?php echo isset($settings['strong']) ? $settings['strong'] : ""; ?>"><?php print $content->title; ?></h3>
    <?php endif ?>
    <div class="pane-content">
      <?php print render($content->content); ?>
    </div>
  </div>
</div>