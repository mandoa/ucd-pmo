<?php

/**
 * Implementation of hook_panels_styles().
 */
$plugin = array(
  'main_content' => array(
    'title' => t('Main Content'),
    'description' => t('Block of the Main Content'),
    'render pane' => 'main_content_render_pane',
    'hook theme' => array(
      'main_content_pane' => array(
        'variables' => array('content' => NULL),
        'path' =>
        drupal_get_path('theme', 'ucd_webcms') . '/panels/styles/main_content',
        'template' => 'main-content-pane',
      ),
    ),
  ),
);

/**
 * Render the Pane
 */
function theme_main_content_render_pane($vars) {
  $content = $vars['content'];
  return theme('main_content_pane', array('content' => $content));
}

