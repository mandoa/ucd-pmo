<div class="panel-pane main-content <?php print isset($content->css_class) ? $content->css_class : ""; ?>" <?php print isset($content->css_id) ? ' id="' . $content->css_id . '"' : ""; ?>>
  <div class="panel-inside">
    <?php if (isset($content->title)): ?>
      <div id="page_title">
        <h2 class="pane-title"><?php print $content->title; ?></h2>
      </div>
    <?php endif ?>
    <div class="pane-content">
      <?php print render($content->content); ?>
    </div>
  </div>
</div>