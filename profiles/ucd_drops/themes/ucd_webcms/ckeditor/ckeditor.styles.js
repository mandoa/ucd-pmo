/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 */
if (typeof(CKEDITOR) !== 'undefined') {
  CKEDITOR.addStylesSet( 'drupal',
  [
    /* Block Styles */
    //{ name : 'Blue Title', element : 'h3', styles : { 'color' : 'Blue' } },
    { name : 'Normal', element : 'p' },
    { name : 'Title', element : 'h3' },
    { name : 'Sub Title', element : 'h4' },
    { name : 'Sub Title 2', element : 'h5' },
    { name : 'Sub Title 3', element : 'h6' },
    { name : 'Clear Aligns', element : 'p', attributes : { 'class' : 'clear-align' } },

    /* Inline Styles */
    /*{ name : 'Red', element : 'span', attributes : { 'class' : 'red-text' } }, */
    { name : 'Button', element : 'a', attributes : { 'class' : 'button' } },
    { name : 'Button Large', element : 'a', attributes : { 'class' : 'button-lg' } },

    /* Object Styles */
    { name : 'Align Left', element : 'img', attributes : { 'class' : 'align-left' } },
    { name : 'Align Right', element : 'img', attributes : { 'class' : 'align-right' } },
  ]);
}