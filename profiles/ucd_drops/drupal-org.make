api = 2
core = 7.x

; Contributed modules.

projects[apachesolr][subdir] = contrib

projects[redis][subdir] = contrib
projects[redis][version] = 2

projects[cas][subdir] = contrib

projects[cas_attributes][subdir] = contrib

projects[ckeditor][subdir] = contrib

projects[ctools][subdir] = contrib

projects[date][subdir] = contrib
projects[date][patch][] = "https://www.drupal.org/files/issues/calendar_pager_broken-2375235-35.patch"

projects[diff][subdir] = contrib

projects[email][subdir] = contrib

projects[entity][subdir] = contrib

projects[entity_view_mode][subdir] = contrib

projects[entityreference][subdir] = contrib

projects[facetapi][subdir] = contrib

projects[features][subdir] = contrib

projects[field_group][subdir] = contrib

projects[fitvids][subdir] = contrib

projects[flag][subdir] = contrib
projects[flag][version] = 2

projects[flexslider][subdir] = contrib

projects[fontyourface][subdir] = contrib

projects[form_builder][subdir] = contrib
projects[form_builder][version] = 1.9

projects[google_analytics][subdir] = contrib

projects[site_verify][subdir] = contrib

projects[honeypot][subdir] = contrib

projects[image_resize_filter][subdir] = contrib

projects[imce][subdir] = contrib

projects[invisimail][subdir] = contrib

projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = 2

projects[ldap][subdir] = contrib
projects[ldap][version] = 1

projects[libraries][subdir] = contrib

projects[link][subdir] = contrib

projects[mail_redirect][subdir] = contrib

projects[menu_block][subdir] = contrib

projects[menu_position][subdir] = contrib

projects[metatag][subdir] = contrib

projects[migrate][subdir] = contrib

projects[module_filter][subdir] = contrib

projects[multiupload_filefield_widget][subdir] = contrib

projects[multiupload_imagefield_widget][subdir] = contrib

projects[nodeblock][subdir] = contrib

projects[options_element][subdir] = contrib

projects[panelizer][subdir] = contrib

projects[panels][subdir] = contrib

projects[pathauto][subdir] = contrib
projects[pathauto][patch][] = "http://drupal.org/files/pathauto-url-pattern-page.patch"

projects[globalredirect][subdir] = contrib

projects[redirect][subdir] = contrib

projects[smtp][subdir] = contrib

projects[strongarm][subdir] = contrib

projects[superfish][subdir] = contrib

projects[token][subdir] = contrib

projects[views][subdir] = contrib

projects[views_slideshow][subdir] = contrib

projects[webform][subdir] = contrib
projects[webform][version] = 4

projects[xmlsitemap][subdir] = contrib

; Developer Contributed Modules

projects[coffee][subdir] = contrib

projects[devel][subdir] = contrib

projects[environment_indicator][subdir] = contrib

projects[masquerade][subdir] = contrib

; Libraries.

libraries[CAS][type] = libraries
libraries[CAS][download][type] = get
libraries[CAS][download][url] = http://downloads.jasig.org/cas-clients/php/1.3.3/CAS-1.3.3.tgz
libraries[CAS][download][subtree] = CAS-1.3.3/

libraries[fitvids][type] = libraries
libraries[fitvids][download][type] = get
libraries[fitvids][download][url] = https://github.com/davatron5000/FitVids.js/archive/master.zip

libraries[flexslider][type] = libraries
libraries[flexslider][download][type] = get
libraries[flexslider][download][url] = https://api.github.com/repos/woothemes/FlexSlider/zipball/version/2.2.2

libraries[ckeditor][type] = libraries
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.5/ckeditor_4.4.5_full.zip
