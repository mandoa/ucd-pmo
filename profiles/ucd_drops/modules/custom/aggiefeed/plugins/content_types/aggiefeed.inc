<?php

$plugin = array(
  'title' => t('AggieFeed'),
  'description' => t('Display a list of activities and news items from across UC Davis.'),
  'single' => TRUE,
  'content_types' => array('aggiefeed'),
  'render callback' => 'aggiefeed_content_type_render',
  'edit form' => 'aggiefeed_content_type_edit_form',
  'category' => t('Blocks'),
);

/**
 * Ctools edit form.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function aggiefeed_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form = aggiefeed_settings_form($form, $conf);

  return $form;
}

/**
 * Ctools edit form submit handler.
 *
 * @param $form
 * @param $form_state
 */
function aggiefeed_content_type_edit_form_submit($form, &$form_state) {
  $conf = _aggiefeed_find_conf($form_state['values']);

  foreach ($conf as $key => $value) {
    $form_state['conf'][$key] = $value;
  }
}

/**
 * Render callback function.
 *
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $context
 * @return stdClass
 */
function aggiefeed_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  // Get the generated html embed code
  $html = aggiefeed_generate_embed($conf);

  $block->content = $html;

  return $block;
}