/**
 * @file
 * Javascript for the AggieFeed
 */
(function ($) {

  // Create an AggieFeed from the embedded code
  Drupal.behaviors.AggieFeedInit = {
    attach: function (context) {

      // Find the first aggiefeed embed and init once. This will init for every
      // embed on the site.
      $('.aggiefeed-element', context).first().once('aggie-init', function () {
        aggieFeed.init();
      });

    }
  };

})(jQuery);