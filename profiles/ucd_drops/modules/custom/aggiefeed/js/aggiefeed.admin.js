/**
 * @file
 * Javascript for the AggieFeed Admin interface
 */
(function ($) {

  // Triggers the CTools autosubmit on the given form. If timeout is passed,
  // it'll set a timeout to do the actual submit rather than calling it directly
  // and return the timer handle.
  function triggerSubmit($form, timeout) {
    var preview_widget = $('#panopoly-form-widget-preview'),
      submit;
    if (!preview_widget.hasClass('panopoly-magic-loading')) {
      preview_widget.addClass('panopoly-magic-loading');
      submit = function () {
        $form.find('.ctools-auto-submit-click').click();
      };
      if (typeof timeout === 'number') {
        return setTimeout(submit, timeout);
      }
      else {
        submit();
      }
    }
  }

// Toggles for the off-canvas tray
  Drupal.behaviors.AggieFeedSlider = {
    attach: function (context, settings) {

      var useSlider = function (input, slider, max) {
        var $slider = $(slider, context),
            $input  = $(input, context),
            $count  = $('<span class="slider-count"></span>', context),
            $form   = $input.closest('form');

        // Hide the input
        $input.hide();

        //Add the count to the end of the Title/label
        $input.prev('label').append($count);

        $slider.once().slider({
          range: "max",
          min: 1,
          max: max,
          value: $input.val(),
          slide: function( event, ui ) {
            $input.val( ui.value );
            $count.text( ui.value );
            triggerSubmit($form, 1000);
          }
        });
        $input.val( $slider.slider('value') );
        $count.text( $slider.slider('value') );
      }


      $('form', context).first().once('aggie-slider', function () {
        useSlider('#edit-af-content-limit', '.content-limit-slider', 512);
        useSlider('#edit-af-num-activities', '.num-activities-slider', 50);
      });

    }
  };

})(jQuery);