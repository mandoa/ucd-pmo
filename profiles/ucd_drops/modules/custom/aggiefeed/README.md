# Summary #

This module allows the AggieFeed embed code to be configured and added into a Drupal website.
https://aggiefeed.ucdavis.edu/

### Installation: For sites with Panels ###

1. Install & enable the AggieFeed module.
2. Add a new Panel Pane and select the "Blocks" category.
3. Add the AggieFeed Pane.

### Installation: For sites using normal Blocks ###

1. Install & enable both AggieFeed and AggieFeed Block module.
2. Go to the Blocks configuration page. /admin/structure/block
3. Add the AggieFeed block to a region and save the blocks page.

### Settings ###

* Configure the settings for each AggieFeed block
* If your site does not use FontAwesome then be sure to set the setting asking if you need it to "Yes".