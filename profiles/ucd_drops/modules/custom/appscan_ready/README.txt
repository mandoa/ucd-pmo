The AppScan Ready module will prepare a site to be scanned by App Scan.

The module is designed to allow a large amount of login attempts so that App
Scan can have sufficient attempts to hack a site.

The Mail Redirect module is used to prevent real email addresses from being
spammed by the system during scans.

Finally, the Honeypot module will be disabled (if currently enabled) so that it
will not prevent form submissions by thinking they are spam attacks. The
Honeypot module will be re-enabled upon disabling the AppScan Ready module.