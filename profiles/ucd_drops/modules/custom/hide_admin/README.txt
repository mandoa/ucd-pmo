README for Hide Admin module.

This module does nothing more than hide a few critical areas from others who 
need to add and edit users.

Use this module if for example you have a content editor who needs to be able 
to add users and perhaps assign new roles to them, but you don't want them to
be able to promote themselves or others to an administrator role. 

To allow them to manage users in this way their role would need the permissions
"Administer permissions" and "Administer users". Unfortunately, this means they
have access to change people's permissions which can mess up the site.

This module checks to see if a user has the permission "administer modules". If
they do then it is assumed that this users is advanced and has access to
critical portions of the website. Thus, they are most likely an administrator.
If the user does not have "administer modules" permission then it hides user 
permissions and the ability to promote to administrator.

CAUTION!
This module assumes the administrator role has an rid of 3.