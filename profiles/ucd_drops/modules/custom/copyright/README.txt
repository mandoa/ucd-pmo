This module is meant to create a block based off of config settings.

This should satisfy Campus policy 310-70
http://manuals.ucdavis.edu/PPM/310/310-70.pdf

The contact and privacy link can be edited on the Site Information page.
/admin/config/system/site-information