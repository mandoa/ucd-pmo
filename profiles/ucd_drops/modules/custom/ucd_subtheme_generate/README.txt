Description
-----------
This is not a true Drupal module but instead is a script for drush to create
subthemes based on the ucd_webcms base theme. With this drush script you can
easily create a subtheme with all the correct components. As well it offers
commands to revert and export theme settings in the .info file.

This script assumes a base theme called ucd_webcms with a starterkit available.
Starterkits are found by the default.starterkit.inc file which will be converted
into the new theme's .info file.

Originally this script was part of the ucd_webcms theme itself but was moved
due to drush not properly caching drush commands within an install profile
theme.



Usage
-----
The following drush commands are available for use:

1. ucd-webcms-wizard
alias ucd-wiz
Guides you through a wizard for generating a subtheme. This is the preferable
method to creating a subtheme.


2. ucd-webcms-subtheme
alias ucd-sub
Creates a UCD WebCMS subtheme.

-- Options --

  destination - The destination of your subtheme. Defaults to "site:all"
  (sites/all/themes). May be one of "site:foo", "profile:bar" or "theme:baz"
  ("foo", "bar" and "baz" being the name of your site, profile or parent theme).
  May also have a third part for sub-pathing. For example, if using
  site:all:custom/cat, then the theme will be created in
  sites/all/themes/custom/cat/<name>.

  machine-name - he machine-readable name of your subtheme. This will be
  auto-generated from the human-readable name if omitted.

  starterkit - The starterkit that your subtheme should use. Defaults to
  "default".

  enable - Automatically enable the subtheme after creation.

  set-default - Automatically enable the subtheme after creation and make it the
  default theme.

  no-readme - Skips readme files when generating the subtheme.

-- Examples --
  drush ucd-webcms-subtheme "My Theme"
  Creates a UCD WebCMS subtheme called "My Theme".

  drush ucd-webcms-subtheme "My Theme" --destination=site:example.com
  Creates a UCD WebCMS subtheme called "My Theme" in sites/example.com/themes.


3. ucd-webcms-export
alias ucd-exp
Exports the theme settings of a given theme from the database to the .info file.

-- Examples --
  drush ucd-webcms-export foo
  Exports the theme settings of the "foo" theme to the "foo.info" file in that
  theme.

  drush ucd-webcms-export foo --revert
  Purges the theme settings of the "foo" theme from the database after exporting
  them to the .info file.


4. ucd-webcms-revert
alias ucd-rev
Reverts the theme settings of a given theme by deleting them from the database.

-- Examples --
  drush ucd-webcms-revert foo
  Reverts the theme settings of the "foo" theme.