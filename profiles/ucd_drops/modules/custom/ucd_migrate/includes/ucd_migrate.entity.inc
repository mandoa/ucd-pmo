<?php

/**
 * @file
 * Populate distribution with some content.
 */

abstract class UCDMigration extends Migration {
  //Entity type suck as "node" or "taxonomy_term"
  public $entityType;

  //entity bundle name such as "page" or "article"
  public $bundle;

  //list of source columns
  public $csvColumns = array();

  //source schema key for tracking the relationships between source rows
  public $sourceKey = array(
    'id' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
    ),
  );

  //path to the directory where files will be imported from
  public $filePath;

  //list of fields that will import multiple separated values such as taxonomy tags
  public $multiValue = array();

  //separator for lists with multiple values
  public $separator = ',';

  //separator to be used within the main separator
  public $subSeparator = '|';

  public function __construct() {
    parent::__construct();

    // Make sure we can use it for node and term only.
    if (!in_array($this->entityType, array('node', 'taxonomy_term'))) {
      throw new Exception('UCDMigration supports only nodes and taxonomy terms.');
    }

    //default file path
    $this->filePath = file_default_scheme() . '://' . 'import';

    $this->description = t('Import @type - @bundle from CSV file.', array('@type' => $this->entityType, '@bundle' => $this->bundle));

    $this->csvColumns = !empty($this->csvColumns) ? $this->csvColumns : array();

    $csv_cols = array();
    if ($this->entityType == 'node') {
      $class_name = 'MigrateDestinationNode'; //use Node migration class
      $csv_cols[] = array('id', 'Unique ID');
      //if using workbench moderation, set the state to published
      if (module_exists('workbench_moderation')) {
        $this->addFieldMapping('workbench_moderation_state_new', 'workbench_moderation_state_new')->defaultValue('published');
      }
    } else if ($this->entityType == 'taxonomy_term') {
      $class_name = 'MigrateDestinationTerm'; //use taxonomy term migration class
      $csv_cols[] = array('name', 'Name');
      $this->addFieldMapping('name', 'name');

      //Use the taxonomy term name as the mapping key
      $this->sourceKey = array(
        'name' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      );
    }

    // Rebuild the csv columns array.
    $this->csvColumns = array_merge($csv_cols, $this->csvColumns);

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName, $this->sourceKey, $class_name::getKeySchema());

    //setup csv migrate options
    $options = array();
    $options['header_rows'] = 1;
    //Do not count multiple line breaks in a field
    $options['embedded_newlines'] = TRUE;
    //$options['delimiter'] = "\t";

    // Create a MigrateSource object.
    $csv_path = $this->filePath . '/csv/' . $this->entityType . '/' . $this->bundle . '.csv';
    $this->source = new MigrateSourceCSV($csv_path, $this->csvColumns, $options);
    $this->destination = new $class_name($this->bundle, array('text_format' => 'standard'));
  }

  //alter a row's content before it imports
  public function prepareRow($row) {
    //re-encode body text to utf-8
    $this->textEncode($row);

    //strip whitespace trailing multiple separated values
    $this->stripTrailingWhitespace($row);

    //get the links and separate them into title and url
    $this->separateLinks($row);

    return true;
  }

  /**
   * strip whitespace after commas
   *
   * @param object $row
   *   The current row being processed
   */
  public function stripTrailingWhitespace ($row) {
    //see if there are any fields will comma separated values
    if (!empty($this->multiValue)) {
      //loop throught the available fields
      foreach ($this->multiValue as $field) {
        //if that field is given in the available row
        if (isset($row->$field)) {
          //strip white space after commas
          $pattern = "/" . $this->separator . "\s*/";
          $row->$field = preg_replace($pattern, $this->separator, $row->$field);
        }
      }
    }
    return true;
  }

  //re-encode body text to utf-8
  public function textEncode($row) {
    //re-encode body text to utf-8
    if (isset($row->body)) {
      $row->body = drupal_convert_to_utf8($row->body, 'WINDOWS-1252');
    }
  }

  /**
   * separate links into Title and Url if a link is separated by a pipe |
   *
   * @param object $row
   *   The current row being processed by reference
   */
  public function separateLinks(&$row) {
    if (isset($row->links)) {

      $titles = array();
      $urls = array();

      $links = explode($this->separator, $row->links);
      $pattern_separator = "\\" . $this->subSeparator;

      foreach($links as $link) {
        //check to see if a pipe | is used resulting in a title prefix
        if (preg_match("/" . $pattern_separator. "http/", $link)) {
          $split = explode($this->subSeparator . "http", $link);
          $titles[] = $split[0];
          $urls[] = 'http' . $split[1];
        } else if (preg_match("/" . $pattern_separator. "sites/", $link)) {
          $split = explode($this->subSeparator . "sites", $link);
          $titles[] = $split[0];
          $urls[] = 'sites' . $split[1];
        } else {
          $titles[] = "";
          $urls[] = $link;
        }
      }

      //put the arrays back into comma separated strings
      $row->links_title = implode($this->separator, $titles);
      $row->links_url = implode($this->separator, $urls);
    }
  }
}
