<?php

/**
 * Migrate Tags terms.
 */
class UCDTagsTermsMigration extends CltcMigration {
  public $entityType = 'taxonomy_term';
  public $bundle = 'tags';
}
