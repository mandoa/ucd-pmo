<?php


/**
 * Migrate Basic Page nodes.
 */
class UCDPageMigration extends UCDMigration {
  public $entityType = 'node';
  public $bundle = 'page';

  //csv columns
  public $csvColumns = array(
    array('title', 'Name'),
    array('body', 'Body'),
    array('image', 'Main Image'),
  );
  
  //add field mappings
  public function __construct() {
    parent::__construct();

    // Assign mappings TO destination fields FROM source fields.
    $this->addFieldMapping('title', 'title');
    
    $this->addFieldMapping('body', 'body')->callbacks('_filter_autop');
    
    $this->addFieldMapping('field_image', 'image');
    $this->addFieldMapping('field_image:file_replace')->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_image:source_dir')->defaultValue($this->filePath . '/images');
  }

}

/**
 * Migrate Article nodes.
 */
class UCDArticleMigration extends UCDMigration {
  public $entityType = 'node';
  public $bundle = 'article';
  
  //these fields have comma separated values (use drupal field name)
  public $multiValue = array(
    'tags',
  );
  
  //these migrate classes are required for term or entity relations
  public $dependencies = array(
    'UCDTagsTerms',
  );

  //csv columns
  public $csvColumns = array(
    array('title', 'Title'),
    array('body', 'Body'),
    array('image', 'Main Image'),
    array('tags', 'Tags'),
  );
  
  //add field mappings
  public function __construct() {
    parent::__construct();

    // Assign mappings TO destination fields FROM source fields.
    $this->addFieldMapping('title', 'title');

    $this->addFieldMapping('body', 'body')->callbacks('_filter_autop');

    $this->addFieldMapping('field_image', 'image');
    $this->addFieldMapping('field_image:file_replace')->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_image:source_dir')->defaultValue($this->filePath . '/images');

    $this->addFieldMapping('field_tags', 'tags')
         ->sourceMigration('UCDTagsTerms')
         ->arguments(array('source_type' => 'tid'))
         ->separator($this->separator);
  }

}
