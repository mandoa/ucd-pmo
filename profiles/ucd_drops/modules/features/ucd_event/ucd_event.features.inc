<?php
/**
 * @file
 * ucd_event.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucd_event_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ucd_event_node_info() {
  $items = array(
    'ucd_event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Create an <em>event</em> which will be tied to a calendar date. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
