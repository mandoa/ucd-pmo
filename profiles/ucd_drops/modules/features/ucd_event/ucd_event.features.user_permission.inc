<?php
/**
 * @file
 * ucd_event.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_event_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_event content'.
  $permissions['create ucd_event content'] = array(
    'name' => 'create ucd_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_event content'.
  $permissions['delete any ucd_event content'] = array(
    'name' => 'delete any ucd_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_event content'.
  $permissions['delete own ucd_event content'] = array(
    'name' => 'delete own ucd_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_event content'.
  $permissions['edit any ucd_event content'] = array(
    'name' => 'edit any ucd_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_event content'.
  $permissions['edit own ucd_event content'] = array(
    'name' => 'edit own ucd_event content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
