<?php
/**
 * @file
 * ucd_nodeblock.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucd_nodeblock_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ucd_nodeblock_node_info() {
  $items = array(
    'ucd_nodeblock' => array(
      'name' => t('Block'),
      'base' => 'node_content',
      'description' => t('Add a block of random content which would not fit in an already defined area of the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
