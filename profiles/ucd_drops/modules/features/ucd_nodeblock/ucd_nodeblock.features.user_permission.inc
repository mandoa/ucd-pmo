<?php
/**
 * @file
 * ucd_nodeblock.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_nodeblock_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_nodeblock content'.
  $permissions['create ucd_nodeblock content'] = array(
    'name' => 'create ucd_nodeblock content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_nodeblock content'.
  $permissions['delete any ucd_nodeblock content'] = array(
    'name' => 'delete any ucd_nodeblock content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_nodeblock content'.
  $permissions['delete own ucd_nodeblock content'] = array(
    'name' => 'delete own ucd_nodeblock content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_nodeblock content'.
  $permissions['edit any ucd_nodeblock content'] = array(
    'name' => 'edit any ucd_nodeblock content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_nodeblock content'.
  $permissions['edit own ucd_nodeblock content'] = array(
    'name' => 'edit own ucd_nodeblock content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
