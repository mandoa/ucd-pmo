<?php
/**
 * @file
 * ucd_article.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_article_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_article content'.
  $permissions['create ucd_article content'] = array(
    'name' => 'create ucd_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_article content'.
  $permissions['delete any ucd_article content'] = array(
    'name' => 'delete any ucd_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_article content'.
  $permissions['delete own ucd_article content'] = array(
    'name' => 'delete own ucd_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_article content'.
  $permissions['edit any ucd_article content'] = array(
    'name' => 'edit any ucd_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_article content'.
  $permissions['edit own ucd_article content'] = array(
    'name' => 'edit own ucd_article content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
