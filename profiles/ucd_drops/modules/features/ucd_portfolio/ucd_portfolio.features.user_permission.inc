<?php
/**
 * @file
 * ucd_portfolio.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_portfolio_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_portfolio content'.
  $permissions['create ucd_portfolio content'] = array(
    'name' => 'create ucd_portfolio content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_portfolio content'.
  $permissions['delete any ucd_portfolio content'] = array(
    'name' => 'delete any ucd_portfolio content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_portfolio content'.
  $permissions['delete own ucd_portfolio content'] = array(
    'name' => 'delete own ucd_portfolio content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_portfolio content'.
  $permissions['edit any ucd_portfolio content'] = array(
    'name' => 'edit any ucd_portfolio content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_portfolio content'.
  $permissions['edit own ucd_portfolio content'] = array(
    'name' => 'edit own ucd_portfolio content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
