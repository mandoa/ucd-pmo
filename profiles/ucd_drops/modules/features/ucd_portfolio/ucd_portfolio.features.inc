<?php
/**
 * @file
 * ucd_portfolio.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucd_portfolio_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ucd_portfolio_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function ucd_portfolio_image_default_styles() {
  $styles = array();

  // Exported image style: portfolio_large.
  $styles['portfolio_large'] = array(
    'name' => 'portfolio_large',
    'label' => 'Portfolio Large',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1300,
          'height' => 731,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: portfolio_thumbnail.
  $styles['portfolio_thumbnail'] = array(
    'name' => 'portfolio_thumbnail',
    'label' => 'Portfolio Thumbnail',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 169,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ucd_portfolio_node_info() {
  $items = array(
    'ucd_portfolio' => array(
      'name' => t('Portfolio'),
      'base' => 'node_content',
      'description' => t('Project portfolio to showcase web sites and web applications built by IET.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
