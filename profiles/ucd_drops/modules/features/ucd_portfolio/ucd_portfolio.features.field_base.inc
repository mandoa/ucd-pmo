<?php
/**
 * @file
 * ucd_portfolio.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ucd_portfolio_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_ucd_portfolio_gallery'
  $field_bases['field_ucd_portfolio_gallery'] = array(
    'active' => 1,
    'cardinality' => 10,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ucd_portfolio_gallery',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  return $field_bases;
}
