<?php
/**
 * @file
 * ucd_portfolio.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ucd_portfolio_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ucd_portfolio';
  $view->description = 'Main Portfolio page listing of projects.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Portfolio';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Portfolio';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'portfolio';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_ucd_portfolio_gallery' => 'field_ucd_portfolio_gallery',
  );
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_class'] = 'portfolio__title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Image Gallery */
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['id'] = 'field_ucd_portfolio_gallery';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['table'] = 'field_data_field_ucd_portfolio_gallery';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['field'] = 'field_ucd_portfolio_gallery';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['label'] = '';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['settings'] = array(
    'image_style' => 'portfolio_thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_ucd_portfolio_gallery']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ucd_portfolio' => 'ucd_portfolio',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'portfolio';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Portfolio';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['ucd_portfolio'] = $view;

  return $export;
}
