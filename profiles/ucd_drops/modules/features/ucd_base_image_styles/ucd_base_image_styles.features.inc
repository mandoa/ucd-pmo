<?php
/**
 * @file
 * ucd_base_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function ucd_base_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: main_photo.
  $styles['main_photo'] = array(
    'name' => 'main_photo',
    'effects' => array(
      1 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 380,
          'height' => 550,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'main_photo',
  );

  // Exported image style: teaser.
  $styles['teaser'] = array(
    'name' => 'teaser',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 75,
          'height' => 75,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'teaser',
  );

  return $styles;
}
