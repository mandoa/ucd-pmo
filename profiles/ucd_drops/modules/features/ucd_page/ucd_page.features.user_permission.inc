<?php
/**
 * @file
 * ucd_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_page content'.
  $permissions['create ucd_page content'] = array(
    'name' => 'create ucd_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_page content'.
  $permissions['delete any ucd_page content'] = array(
    'name' => 'delete any ucd_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_page content'.
  $permissions['delete own ucd_page content'] = array(
    'name' => 'delete own ucd_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_page content'.
  $permissions['edit any ucd_page content'] = array(
    'name' => 'edit any ucd_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_page content'.
  $permissions['edit own ucd_page content'] = array(
    'name' => 'edit own ucd_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
