<?php
/**
 * @file
 * ucd_pantheon_solr_search.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function ucd_pantheon_solr_search_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'localhost server';
  $environment->url = 'http://localhost:8983/solr';
  $environment->service_class = '';
  $environment->conf = array(
    'apachesolr_search_changed_boost' => '0:0',
    'apachesolr_search_comment_boost' => '0:0',
    'apachesolr_search_date_boost' => '4:200.0',
    'apachesolr_search_facet_pages' => '',
    'apachesolr_search_promote_boost' => '0',
    'apachesolr_search_sticky_boost' => '0',
    'apachesolr_search_type_boosts' => array(
      'article' => '21.0',
      'page' => '13.0',
      'block' => '0',
      'event' => '5.0',
      'profile' => '5.0',
      'project' => '8.0',
      'publication' => '13.0',
      'sponsor' => '1.0',
    ),
    'field_bias' => array(
      'content' => '8.0',
      'label' => '13.0',
      'tags_a' => '0',
      'tags_h1' => '5.0',
      'tags_h2_h3' => '3.0',
      'tags_h4_h5_h6' => '2.0',
      'tags_inline' => '1.0',
      'taxonomy_names' => '2.0',
      'tos_content_extra' => '0.1',
      'tos_name' => '3.0',
      'ts_comments' => '0.5',
    ),
  );
  $environment->index_bundles = array(
    'node' => array(
      0 => 'ucd_cntr_article',
      1 => 'ucd_cntr_event',
      2 => 'ucd_cntr_page',
      3 => 'ucd_cntr_profile',
      4 => 'ucd_cntr_project',
      5 => 'ucd_cntr_publication',
      6 => 'ucd_cntr_sponsor',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
