<?php
/**
 * @file
 * ucd_slideshow.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ucd_slideshow_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ucd_slideshow_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function ucd_slideshow_image_default_styles() {
  $styles = array();

  // Exported image style: slideshow.
  $styles['slideshow'] = array(
    'name' => 'slideshow',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1170,
          'height' => 450,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'slideshow',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function ucd_slideshow_node_info() {
  $items = array(
    'ucd_slideshow' => array(
      'name' => t('Slideshow Slide'),
      'base' => 'node_content',
      'description' => t('Add a slideshow image to the home page and optionally link it to another page when clicked.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
