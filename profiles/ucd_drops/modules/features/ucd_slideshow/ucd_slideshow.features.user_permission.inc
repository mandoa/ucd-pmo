<?php
/**
 * @file
 * ucd_slideshow.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ucd_slideshow_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create ucd_slideshow content'.
  $permissions['create ucd_slideshow content'] = array(
    'name' => 'create ucd_slideshow content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any ucd_slideshow content'.
  $permissions['delete any ucd_slideshow content'] = array(
    'name' => 'delete any ucd_slideshow content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own ucd_slideshow content'.
  $permissions['delete own ucd_slideshow content'] = array(
    'name' => 'delete own ucd_slideshow content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any ucd_slideshow content'.
  $permissions['edit any ucd_slideshow content'] = array(
    'name' => 'edit any ucd_slideshow content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own ucd_slideshow content'.
  $permissions['edit own ucd_slideshow content'] = array(
    'name' => 'edit own ucd_slideshow content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
