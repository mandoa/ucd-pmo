<?php
/**
 * @file
 * ucd_slideshow.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ucd_slideshow_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-ucd_slideshow-field_ucd_slideshow_image'
  $field_instances['node-ucd_slideshow-field_ucd_slideshow_image'] = array(
    'bundle' => 'ucd_slideshow',
    'deleted' => 0,
    'description' => 'Upload the photo which will be used in the slideshow.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'slideshow',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ucd_slideshow_image',
    'label' => 'Slide Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'photos/slideshow',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-ucd_slideshow-field_ucd_slideshow_link'
  $field_instances['node-ucd_slideshow-field_ucd_slideshow_link'] = array(
    'bundle' => 'ucd_slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Add a link to another website or page in this site. When someone clicks the slide image they will be taken to this page.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ucd_slideshow_link',
    'label' => 'Link',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_label_use_field_label' => FALSE,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-ucd_slideshow-field_ucd_slideshow_subtitle'
  $field_instances['node-ucd_slideshow-field_ucd_slideshow_subtitle'] = array(
    'bundle' => 'ucd_slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This subtitle will show above the slide image just under the Title.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ucd_slideshow_subtitle',
    'label' => 'Subtitle',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add a link to another website or page in this site. When someone clicks the slide image they will be taken to this page.');
  t('Link');
  t('Slide Image');
  t('Subtitle');
  t('This subtitle will show above the slide image just under the Title.');
  t('Upload the photo which will be used in the slideshow.');

  return $field_instances;
}
