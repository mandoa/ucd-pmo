<?php
/**
 * @file
 * ucd_slideshow.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ucd_slideshow_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ucd_slideshow';
  $view->description = 'A responsive slideshow for the home page';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'homepage';
  $handler->display->display_options['style_options']['captionfield'] = 'title';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: link start - hidden */
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['id'] = 'field_ucd_slideshow_link';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['table'] = 'field_data_field_ucd_slideshow_link';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['field'] = 'field_ucd_slideshow_link';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['ui_name'] = 'link start - hidden';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['label'] = '';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['alter']['text'] = '<a href="[field_ucd_slideshow_link]">';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_ucd_slideshow_link']['type'] = 'link_plain';
  /* Field: link end - hidden */
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['id'] = 'field_ucd_slideshow_link_1';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['table'] = 'field_data_field_ucd_slideshow_link';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['field'] = 'field_ucd_slideshow_link';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['ui_name'] = 'link end - hidden';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['label'] = '';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['alter']['text'] = '</a>';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_ucd_slideshow_link_1']['type'] = 'link_plain';
  /* Field: Subtitle - hidden */
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['id'] = 'field_ucd_slideshow_subtitle';
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['table'] = 'field_data_field_ucd_slideshow_subtitle';
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['field'] = 'field_ucd_slideshow_subtitle';
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['ui_name'] = 'Subtitle - hidden';
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['label'] = '';
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_subtitle']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[field_ucd_slideshow_link]
<span class="title">[title]</span>
<span class="subtitle">[field_ucd_slideshow_subtitle]</span>
[field_ucd_slideshow_link_1]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_class'] = 'flex-caption';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Slide Image */
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['id'] = 'field_ucd_slideshow_image';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['table'] = 'field_data_field_ucd_slideshow_image';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['field'] = 'field_ucd_slideshow_image';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['label'] = '';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['alter']['text'] = '[field_ucd_slideshow_link][field_ucd_slideshow_image][field_ucd_slideshow_link_1]';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ucd_slideshow_image']['settings'] = array(
    'image_style' => 'slideshow',
    'image_link' => '',
  );
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ucd_slideshow' => 'ucd_slideshow',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'clearfix';
  $handler->display->display_options['block_description'] = 'Slideshow';
  $export['ucd_slideshow'] = $view;

  return $export;
}
