This is a set of Features (as in, uses the Features module) that provides remote
authentication functionality via UC Davis servers.

ucd_auth_cas
Provides basic CAS server settings to log in users via CAS. This module can be
enabled on its own.

ucd_auth_ldap
Adds ldap.ucdavis.edu as a server under the LDAP module. In and of itself, it
does very little.

ucd_auth_ad
Adds  UC Davis Active Directory as a server under the LDAP module. In and of
itself, it does very little.

ucd_auth_cas_ldap
Creates user fields and imports UCD LDAP values into them upon login. It is set
to overwrite any values in Drupal with values from LDAP, so it doesn't do any
good to edit them.

Fields:
displayName --> Drupal username
mail --> Drupal email address
edupersonaffiliation --> Affiliation (ie. student, staff, faculty)
ou --> Department
street --> Office
telephonenumber --> Phone
title --> Title