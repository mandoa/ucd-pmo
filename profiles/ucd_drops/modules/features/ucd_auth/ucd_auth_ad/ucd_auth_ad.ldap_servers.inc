<?php
/**
 * @file
 * ucd_auth_ad.ldap_servers.inc
 */

/**
 * Implements hook_default_ldap_servers().
 */
function ucd_auth_ad_default_ldap_servers() {
  $export = array();

  $ldap_servers_conf = new stdClass();
  $ldap_servers_conf->disabled = FALSE; /* Edit this to true to make a default ldap_servers_conf disabled initially */
  $ldap_servers_conf->api_version = 1;
  $ldap_servers_conf->sid = 'ucd_auth_ad';
  $ldap_servers_conf->name = 'UCD Active Directory';
  $ldap_servers_conf->status = TRUE;
  $ldap_servers_conf->ldap_type = 'ad';
  $ldap_servers_conf->address = 'addc1c.ad3.ucdavis.edu';
  $ldap_servers_conf->port = 389;
  $ldap_servers_conf->tls = FALSE;
  $ldap_servers_conf->bind_method = TRUE;
  $ldap_servers_conf->binddn = 'CN=[USERNAME],OU=[SOME_UNIT],OU=[MORE_UNITS],OU=[ETC],DC=ou,DC=ad3,DC=ucdavis,DC=edu';
  $ldap_servers_conf->bindpw = 'password';
  $ldap_servers_conf->basedn = array(
    0 => 'OU=ucdUsers,DC=ad3,DC=ucdavis,DC=edu',
  );
  $ldap_servers_conf->user_attr = 'sAMAccountName';
  $ldap_servers_conf->account_name_attr = '';
  $ldap_servers_conf->mail_attr = 'mail';
  $ldap_servers_conf->mail_template = '';
  $ldap_servers_conf->allow_conflicting_drupal_accts = FALSE;
  $ldap_servers_conf->unique_persistent_attr = '';
  $ldap_servers_conf->user_dn_expression = '';
  $ldap_servers_conf->ldap_to_drupal_user = '';
  $ldap_servers_conf->testing_drupal_username = '';
  $ldap_servers_conf->group_object_category = 'group';
  $ldap_servers_conf->search_pagination = FALSE;
  $ldap_servers_conf->search_page_size = 1000;
  $ldap_servers_conf->weight = 0;
  $export['ucd_auth_ad'] = $ldap_servers_conf;

  return $export;
}
