<?php
/**
 * @file
 * ucd_auth_cas_ldap.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ucd_auth_cas_ldap_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cas_attributes_ldap_server';
  $strongarm->value = 'ucd_auth_ldap';
  $export['cas_attributes_ldap_server'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cas_attributes_relations';
  $strongarm->value = array(
    'name' => '[cas:ldap:displayname]',
    'mail' => '[cas:ldap:mail]',
    'field_ucd_auth_cas_ldap_affil' => '[cas:ldap:edupersonaffiliation]',
    'field_ucd_auth_cas_ldap_dept' => '[cas:ldap:ou]',
    'field_ucd_auth_cas_ldap_office' => '[cas:ldap:postaladdress]',
    'field_ucd_auth_cas_ldap_phone' => '[cas:ldap:telephonenumber]',
    'field_ucd_auth_cas_ldap_title' => '[cas:ldap:title]',
  );
  $export['cas_attributes_relations'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cas_attributes_sync_every_login';
  $strongarm->value = '1';
  $export['cas_attributes_sync_every_login'] = $strongarm;

  return $export;
}
