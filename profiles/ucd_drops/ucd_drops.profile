<?php
function ucd_drops_install_tasks($install_state) {
  $tasks = array (
    'ucd_drops_configure' => array(),
  );
  return $tasks;
}

/**
 * Set up base config
 */
function ucd_drops_configure() {
  // Set default Pantheon variables
  variable_set('cache', 1);
  variable_set('block_cache', 1);
  variable_set('cache_lifetime', '0');
  variable_set('page_cache_maximum_age', '900');
  variable_set('page_compression', 0);
  variable_set('preprocess_css', 1);
  variable_set('preprocess_js', 1);
  $search_active_modules = array(
    'apachesolr_search' => 'apachesolr_search',
    'user' => 'user',
    'node' => 0
  );
  variable_set('search_active_modules', $search_active_modules);
  variable_set('search_default_module', 'apachesolr_search');
  //drupal_set_message(t('Pantheon defaults configured.'));
  
  //revert features when installation is complete
  if (module_exists('ucd_wysiwyg')) {
    features_revert(array(
      'ucd_wysiwyg' => array('ckeditor_profile', 'user_permission'),
      'ucd_page' => array('user_permission'),
    ));
  }
  
  $enable = array(
    'theme_default' => 'ucd_webcms',
    'admin_theme' => 'seven',
    //'zen'
  );
  theme_enable($enable);

  foreach ($enable as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }
}

/**
 * Set UCD Drops as default install profile.
 *
 * Must use system as the hook module because ucd_drops is not active yet
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'ucd_drops';
  }
}

/**
 * Implements hook_install_tasks_alter().
 */
function ucd_drops_install_tasks_alter(&$tasks, $install_state) {
  // Preselect the English language, so we can skip the language selection form.
  if (!isset($_GET['locale'])) {
    $_POST['locale'] = 'en';
  }
}

/**
 * Clear all 'notification' type messages that may have been set.
 */
function ucd_drops_clear_messages() {
  drupal_get_messages('status', TRUE);
  drupal_get_messages('warning', TRUE);
  drupal_get_messages('completed', TRUE);
}



// == Configuration UI =========================================================

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function ucd_drops_form_install_configure_form_alter(&$form, $form_state) {
  // Suppress messages from installed modules, except errors.
  ucd_drops_clear_messages();

  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  // Set a default country
  $form['server_settings']['site_default_country']['#default_value'] = 'US';
}

/**
 * Implements hook_date_format_types().
 *
 * Adds date types for month and day.
 */
function ucd_drops_date_format_types() {
  // Define the core date format types.
  return array(
    'month' => t('Month'),
    'day' => t('Day'),
    'time' => t('Time'),
  );
}

/**
 * Implements hook_date_formats().
 */
function ucd_drops_date_formats() {
  return array(
    array(
      'type' => 'short',
      'format' => 'm/d/Y',
      'locales' => array(),
    ),
    array(
      'type' => 'month',
      'format' => 'M',
      'locales' => array(),
    ),
    array(
      'type' => 'month',
      'format' => 'n',
      'locales' => array(),
    ),
    array(
      'type' => 'month',
      'format' => 'F',
      'locales' => array(),
    ),
    array(
      'type' => 'day',
      'format' => 'j',
      'locales' => array(),
    ),
    array(
      'type' => 'day',
      'format' => 'jS',
      'locales' => array(),
    ),
    array(
      'type' => 'day',
      'format' => 'D',
      'locales' => array(),
    ),
    array(
      'type' => 'day',
      'format' => 'l',
      'locales' => array(),
    ),
    array(
      'type' => 'time',
      'format' => 'g:ia',
      'locales' => array(),
    ),
    array(
      'type' => 'time',
      'format' => 'G:i',
      'locales' => array(),
    ),
    array(
      'type' => 'time',
      'format' => 'H:i',
      'locales' => array(),
    ),
  );
}